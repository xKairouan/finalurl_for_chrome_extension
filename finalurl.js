// -*- coding: utf-8 -*-

(function () {
    'use strict';

    var removeFlagment = function (url) {
        return url.replace(/#.+$/, '');
    };

    var requestFilter = {
        urls: ['http://*/*', 'https://*/*'],
        types: ['xmlhttprequest']
    };
    var requestUrls = {};
    var finalUrls = {};
    chrome.webRequest.onBeforeRedirect.addListener(function (details) {
        var requestId = details.requestId;
        if (requestId in requestUrls) {
            finalUrls[requestUrls[requestId]] = details.redirectUrl;
            return;
        }
        requestUrls[requestId] = details.url;
        finalUrls[details.url] = details.redirectUrl;
    }, requestFilter);
    chrome.webRequest.onCompleted.addListener(function (details) {
        delete requestUrls[details.requestId];
    }, requestFilter);

    chrome.extension.onRequest.addListener(function (request, sender, sendResponse) {
        switch (request.message) {
        case 'finalUrl':
            var url = removeFlagment(request.url);
            if (url in finalUrls) {
                sendResponse({finalUrl: finalUrls[url]});
            }
            else {
                sendResponse({finalUrl: url});
            }
            break;
        default:
            break;
        }
    });
}());
