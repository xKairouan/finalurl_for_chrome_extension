// -*- coding: utf-8 -*-

(function () {
    'use strict';

    var URL = function (url) {
        var a = document.createElement('a');
        a.href = url;
        this.protocol = a.protocol;
        this.host = a.host;
    };
    var isSameOrigin = function (lhs, rhs) {
        lhs = new URL(lhs);
        rhs = new URL(rhs);
        return lhs.protocol === rhs.protocol && lhs.host === rhs.host;
    };

    var next = document.querySelector('a[rel="next"]');
    if (!next) {
        return;
    };
    var url = next.href;
    var req = new XMLHttpRequest();
    req.addEventListener('load', function (e) {
        var request = {
            message: 'finalUrl',
            url: url
        };
        var callback = function (response) {
            if (!isSameOrigin(location.href, response.finalUrl)) {
                alert('blocked! ' + response.finalUrl);
                return;
            }
            alert(e.target.responseText.substr(0, 500));
        };
        chrome.extension.sendRequest(request, callback);
    }, false);
    req.addEventListener('error', function (e) {
        alert('Error!');
    }, false);
    req.open('GET', url);
    req.send(null);
}());
